# # encoding: utf-8

# Inspec test for recipe mywrapper_chef_client_updater::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe command('chef-client --version') do
  target_version = '16'
  its('stdout') { should match "^Chef Infra Client: #{target_version}" }
end
